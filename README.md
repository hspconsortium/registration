# SMART on FHIR App Registration #

This project is for collaborating around the topic of registering SMART on FHIR apps with "registration systems" such as development sandboxes, community galleries, vendor integration environments, vendor app stores.

## Goals ##

* An app developer can easily declare registration and launch information a single time for their app.
* An app developer can easily submit an application to registration systems.

## System Participants ##

* Hosted SMART App: A SMART on FHIR app that is hosted in the public Internet.
* Packaged SMART App: A SMART on FHIR app that is bundled and transferred to a registering participant.
* Native SMART App: A SMART on FHIR app that is running natively on a device.
* Sandbox Registry: A development system that is able to launch a SMART on FHIR app.
* Gallery Registry: A showcasing system that is able to launch a SMART on FHIR app.
* EHR Registry: A EHR system that is able to launch a SMART on FHIR app.
* Store Registry: An App Store system that is able to launch a SMART on FHIR app.

## Registration Information ##

The app will provide a metadata.json file that will contain information required to allow a registering participant to create an OAuth client for the app and SMART on FHIR embedded or standalone launch flow.

## metadata.json ##

### Location ###

| System Participant | Location                         |
| ------------------ | -------------------------------- |
| Hosted SMART App   | /.well-known/smart/manifest.json |
| Packaged SMART App | /.well-known/smart/manifest.json |
| Native SMART App   | TBD                              |

### Content ###

```
{
  "software_id": "org.hspconsortium.bilirubin:bilirubin-risk-chart:1.0-SNAPSHOT",
  "client_name": "Bilirubin Risk Chart",
  "client_uri": "https://apps.hspconsortium.org/hspc-bilirubin-risk-chart/index.html",
  "logo_uri": "https://apps.hspconsortium.org/hspc-bilirubin-risk-chart/static/bilirubin-chart/images/bilirubin.png",
  "launch_url": "https://apps.hspconsortium.org/hspc-bilirubin-risk-chart/static/bilirubin-chart/launch.html",
  "redirect_uris": [
    "https://apps.hspconsortium.org/hspc-bilirubin-risk-chart/static/bilirubin-chart/index.html"
  ],
  "scope": "launch online_access patient/Patient.read patient/Observation.read patient/Observation.write",
  "token_endpoint_auth_method": "none",
  "grant_types": [
    "authorization_code"
  ],
  "fhir_versions": [
    "1.0.2", "1.8.0"
  ]
}
```

| Property           | Datatype                         | Description    | See |
| ------------------ | -------------------------------- | --- | ---|
| software_id   | text | Uniquely identifies the app.  Can be consistent across registering systems.  The example uses Maven coordinates. | |
| client_name | text | Human readable OAuth 2 client name. | https://tools.ietf.org/html/rfc7591 |
| client_uri  | URL | Absolute URL of a web page providing information about the client. | https://tools.ietf.org/html/rfc7591 |
| logo_uri  | URL | Absolute URL of a client logo to display to the end user. | http://docs.smarthealthit.org/authorization/ |
| launch_url  | URL | SMART on FHIR launch url as a absolute URL. | http://docs.smarthealthit.org/authorization/ |
| redirect_uris  | URL | Array of absolute redirection URI strings for use in redirect-based flows such as the authorization code and implicit flows. | https://tools.ietf.org/html/rfc7591 |
| scope  | text | A space-separated list of scope values. | http://docs.smarthealthit.org/authorization/ |
| token_endpoint_auth_method | text | String indicator of the requested authentication method for the token endpoint.  Values are: "none", "client_secret_post", and "client_secret_basic". | https://tools.ietf.org/html/rfc7591 |
| grant_types  | text | Array of OAuth 2.0 grant types that the client may use. | http://docs.smarthealthit.org/authorization/ |
| fhir_versions  | text | Array of text containing FHIR version numbers that this application supports. | http://hl7.org/fhir/directory.html |

### Extensions ###

### Comparisons ###

## Workflow ##
