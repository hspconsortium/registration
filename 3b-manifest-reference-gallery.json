{
  "client": {
    "type": "public",
    "name": "Bilirubin SMART sandbox client",
    "description": "Client used by the Bilirubin Risk Chart app"
  },
  "app": {
    "id": "15dfd791-3e5f-4f1a-83ef-d5e0ea898a1d",
    "name": "Bilirubin Risk Chart",
    "summary": "Demonstration app designed to help clinicians treat newborn hyperbilirubinemia appropriately.",
    "logo_256x256_png": "https://example.com/1.png",
    "logo_512x512_png": "https://example.com/1@2x.png",
    "oauth_redirect_uri": "https://example.com/bilirubin-risk-chart/index.html",
    "smart_launch_uri": "https://example.com/bilirubin-risk-chart/launch.html",
    "scope": "launch patient/*.read online_access",
    "fhir_version": [
      "1.0.2",
      "1.4",
      "1.8"
    ],
    "user_profile_type": [
      "Practitioner"
    ]
  },
  "organization": {
    "name": "Intermountain Healthcare",
    "description": "Intermountain Healthcare is a not-for-profit health system based in Salt Lake City, Utah. We are the largest healthcare provider in the Intermountain West, with 37,000 employees serving the healthcare needs of people in Utah, southeastern Idaho and surrounding areas.",
    "url": "http://website.com",
    "developer_contact_email": "travis@iSalusSolutions.com",
    "sales_contact_email": "travis@iSalusSolutions.com",
    "logo_256x256_png": "https://example.com/1.png",
    "logo_512x512_png": "https://example.com/1@2x.png",
    "logo_256x64_png": "https://example.com/1.png",
    "logo_512x128_png": "https://example.com/1@2x.png"
  },
  "sample_data": {
    "resourceType": "Bundle",
    "type": "transaction",
    "entry": [
      {
        "resource": {
          "resourceType": "Patient",
          "id": "BILIBABY0",
          "name": [
            {
              "family": [
                "Langenheim"
              ],
              "given": [
                "Ruby"
              ]
            }
          ],
          "birthDate": "2016-02-04T00:00:00",
          "gender": "male",
          "extension": [
            {
              "url": "http://hl7.org/fhir/StructureDefinition/patient-birthTime",
              "valueDateTime": "2016-02-04T00:00:00"
            }
          ],
          "active": "true"
        },
        "request": {
          "method": "PUT",
          "url": "Patient/BILIBABY0"
        }
      },
      {
        "resource": {
          "resourceType": "Observation",
          "id": "BILIBABY0-OBS1",
          "code": {
            "coding": {
              "system": "http://loinc.org",
              "code": "58941-6",
              "display": "Transcutaneous Bilirubin"
            }
          },
          "valueQuantity": {
            "value": "2.0",
            "unit": "mg/dL",
            "code": "mg/dL"
          },
          "effectiveDateTime": "2016-02-04T08:12:00",
          "status": "final",
          "subject": {
            "reference": "Patient/BILIBABY0"
          }
        },
        "request": {
          "method": "PUT",
          "url": "Observation/BILIBABY0-OBS1"
        }
      }
    ]
  },
  "demo_launch_context": [
    {
      "patient": "BILIBABY0"
    }
  ],
  "marketing": {
    "description": "The Intermountain Healthcare Bilirubin Risk Chart App is based on Intermountain’s HELP2 application of the same name. This app is for demonstration purposes only and is not approved for clinical use. It was created to highlight the concept of the HSPC model, which creates a method of sharing clinical applications currently locked up in a single organization or EHR vendor and make it available to all healthcare providers.\n\nJaundice occurs in most newborn infants. Most jaundice is benign, but because of the potential toxicity of bilirubin, newborn infants must be monitored to identify those who might develop severe hyperbilirubinemia and, in rare cases, acute bilirubin encephalopathy or kernicterus. Although kernicterus should almost always be preventable, cases continue to occur. The focus of this app is to reduce the incidence of severe hyperbilirubinemia and bilirubin encephalopathy while minimizing the risks of unintended harm such as maternal anxiety, decreased breastfeeding, and unnecessary costs or treatment.\n\nBy overlaying bilirubin [Mass/volume] results over a time-based risk chart, clinicians are presented with a visual representation of the results and associated criticality zones. By viewing the results in this way, the clinician can determine the best course of action based on the recommended intervention for the criticality of the result.",
    "sales_contact": "",
    "screenshot_1024x768_png": [
      "https://example.com/1.png"
    ],
    "screenshot_2048x1536": [
      "https://example.com/1@2x.png"
    ],
    "video_url": [
      "https://example.com/1.mpeg"
    ],
    "category": [
      "disease-management",
      "calculator",
      "longitudinal"
    ],
    "specialty": [
      "pediatrics"
    ]
  }
}

